import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { selectFilter, setCurrentPage } from "../../redux/slices/filterSlice";
import styles from "./pagination.module.scss";

const Pagination: React.FC = () => {
  const { currentPage } = useSelector(selectFilter);
  const dispatch = useDispatch();
  const onPageClick = (index: number) => {
    if (!index) index = 1;
    if (index >= 4) index = 3;
    dispatch(setCurrentPage(index));
  };
  return (
    <ul className={styles.pagination}>
      <li onClick={() => onPageClick(currentPage - 1)}>&lt;</li>
      <li
        onClick={() => onPageClick(1)}
        className={currentPage === 1 ? styles.active : ""}
      >
        1
      </li>
      <li
        onClick={() => onPageClick(2)}
        className={currentPage === 2 ? styles.active : ""}
      >
        2
      </li>
      <li
        onClick={() => onPageClick(3)}
        className={currentPage === 3 ? styles.active : ""}
      >
        3
      </li>
      <li onClick={() => onPageClick(currentPage + 1)}>&gt;</li>
    </ul>
  );
};

export default Pagination;
