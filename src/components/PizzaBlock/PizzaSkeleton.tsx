import ContentLoader from "react-content-loader";
import React from "react";

const PizzaSkeleton: React.FC = () => (
  <ContentLoader
    className="pizza-block"
    speed={2}
    width={280}
    height={470}
    viewBox="0 0 280 470"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  >
    <circle cx="140" cy="130" r="130" />
    <rect x="0" y="267" rx="10" ry="10" width="280" height="27" />
    <rect x="0" y="314" rx="10" ry="10" width="280" height="88" />
    <rect x="128" y="422" rx="10" ry="10" width="152" height="45" />
    <rect x="0" y="435" rx="10" ry="10" width="90" height="27" />
  </ContentLoader>
);

export default PizzaSkeleton;
