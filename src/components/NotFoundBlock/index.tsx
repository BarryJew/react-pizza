import styles from "./NotFoundBlock.module.scss";
import React from "react";

const NotFoundBlock: React.FC = () => {
  return (
    <div className={styles.root}>
      <h1>
        <span>:(</span>
        <br />
        Страница не найдена
      </h1>
      <p>Данная страница отсутствует на сервере</p>
    </div>
  );
};

export default NotFoundBlock;
