import styles from "./search.module.scss";
import closeSvg from "../../assets/img/close.svg";
import React from "react";
import debounce from "lodash.debounce";
import { useDispatch } from "react-redux";
import { setSearch } from "../../redux/slices/filterSlice";

const Search = () => {
  const dispatch = useDispatch();
  const [inputValue, setInputValue] = React.useState<string>("");
  const inputRef = React.useRef<HTMLInputElement>(null);
  const updateSearch = React.useCallback(
    debounce((str: string) => {
      dispatch(setSearch(str));
    }, 250),
    []
  );
  const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    updateSearch(event.target.value);
  };
  const onClearClick = () => {
    setInputValue("");
    dispatch(setSearch(""));
    inputRef.current?.focus();
  };

  return (
    <div className={styles.root}>
      <input
        ref={inputRef}
        value={inputValue}
        onChange={(event) => onSearchChange(event)}
        placeholder="Поиск пиццы..."
        type="text"
      />
      {inputValue && (
        <img
          onClick={() => onClearClick()}
          className={styles.close}
          src={closeSvg}
          alt=""
        />
      )}
    </div>
  );
};

export default Search;
