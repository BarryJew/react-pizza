import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";

export type FilterSortType = {
  name: string;
  sortProperty: string;
};

interface FilterSliceState {
  categoryId: number;
  sort: FilterSortType;
  currentPage: number;
  search: string;
}

const initialState: FilterSliceState = {
  categoryId: 0,
  sort: {
    name: "популярности (DESC)",
    sortProperty: "-rating",
  },
  currentPage: 1,
  search: "",
};

const filterSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    setCategoryId(state, action) {
      state.categoryId = action.payload;
    },
    setSort(state, action) {
      state.sort = action.payload;
    },
    setCurrentPage(state, action) {
      state.currentPage = action.payload;
    },
    setSearch(state, action) {
      state.search = action.payload;
    },
    setFilters(state, action) {
      state.currentPage = Number(action.payload.currentPage);
      state.categoryId = Number(action.payload.categoryId);
      state.sort = action.payload.sort;
    },
  },
});

export const selectFilter = (state: RootState) => state.filter;

export const { setCategoryId, setSort, setCurrentPage, setSearch, setFilters } =
  filterSlice.actions;

export default filterSlice.reducer;
