import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { getCartStateLS } from "../../utils/getCartStateLS";

export type CartItemType = {
  id: string;
  title: string;
  price: number;
  imageUrl: string;
  type: string;
  size: number;
  count: number;
};

export interface CartSliceState {
  totalPrice: number;
  totalCount: number;
  items: CartItemType[];
}

const initialState: CartSliceState = getCartStateLS();

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addItem(state, action: PayloadAction<CartItemType>) {
      // const { count, ...rest } = action.payload;
      // const findItem = state.items.find((obj) => {
      //   const { count, ...current } = obj;
      //   if (JSON.stringify(rest) === JSON.stringify(current)) return obj;
      // });
      const { id, type, size } = action.payload;
      const findItem = state.items.find((obj) => {
        return obj.id === id && obj.type === type && obj.size === size;
      });
      // const findItem = state.items.find((obj) => obj.id === action.payload.id);

      if (findItem) {
        findItem.count++;
      } else {
        state.items.push({
          ...action.payload,
          count: 1,
        });
      }
      state.totalPrice = state.items.reduce((sum, obj) => {
        return obj.price * obj.count + sum;
      }, 0);
      state.totalCount = state.items.reduce((sum, obj) => {
        return obj.count + sum;
      }, 0);
    },
    removeItem(state, action: PayloadAction<CartItemType>) {
      const { id, type, size } = action.payload;
      const findItem = state.items.find((obj) => {
        return obj.id === id && obj.type === type && obj.size === size;
      });
      if (findItem) {
        state.totalPrice = state.totalPrice - findItem.price;
        state.totalCount--;
        findItem.count--;
      }
    },
    clearItem(state, action: PayloadAction<CartItemType>) {
      const { id, type, size } = action.payload;
      const findItem = state.items.find((obj) => {
        return obj.id === id && obj.type === type && obj.size === size;
      });
      if (findItem) {
        state.totalPrice = state.totalPrice - findItem.price * findItem.count;
        state.totalCount = state.totalCount - findItem.count;
        state.items = state.items.filter((obj) => {
          return JSON.stringify(obj) !== JSON.stringify(findItem);
        });
      }
    },
    clearCart(state) {
      state.items = [];
      state.totalCount = 0;
      state.totalPrice = 0;
    },
  },
});

export const selectCart = (state: RootState) => state.cart;
export const selectItemById =
  (id: string, type: string, size: number) => (state: RootState) =>
    state.cart.items.find((obj) => {
      return obj.id === id && obj.type === type && obj.size === size;
    });

export const { addItem, removeItem, clearItem, clearCart } = cartSlice.actions;

export default cartSlice.reducer;
