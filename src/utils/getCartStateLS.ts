import { CartItemType, CartSliceState } from "../redux/slices/cartSlice";

export const getCartStateLS = () => {
  const json = window.localStorage.getItem("cart");
  if (json) {
    const data: CartItemType[] = JSON.parse(json);
    const cartState: CartSliceState = {
      totalPrice: data.reduce((sum, obj) => {
        return obj.price * obj.count + sum;
      }, 0),
      totalCount: data.reduce((sum, obj) => {
        return obj.count + sum;
      }, 0),
      items: data,
    };
    return cartState;
  }
  return {
    totalPrice: 0,
    totalCount: 0,
    items: [],
  };
};
