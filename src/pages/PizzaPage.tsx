import React from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";

const PizzaPage: React.FC = () => {
  const [pizza, setPizza] = React.useState<{
    title: string;
    imageUrl: string;
    price: number;
  }>();
  const params = useParams();

  const getPizza = async () => {
    try {
      const { data } = await axios.get(
        `https://62e63434de23e2637928d58a.mockapi.io/items/${params.id}`
      );
      setPizza(data);
    } catch (err) {
      console.log(err);
    }
  };
  React.useEffect(() => {
    getPizza();
  }, []);

  if (!pizza) {
    return <>Загрузка</>;
  }

  return (
    <div className="container pizza-page">
      <h1>{pizza.title}</h1>
      <img src={pizza.imageUrl} alt="" />
      <div className="pizza-block__price pizza-page__price">
        от {pizza.price} ₽
      </div>
      <Link to="/" className="button button--black">
        <span>Назад</span>
      </Link>
    </div>
  );
};

export default PizzaPage;
