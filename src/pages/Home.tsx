import React from "react";
import qs from "qs";
import { useSearchParams } from "react-router-dom";
import {
  Categories,
  Sort,
  PizzaSkeleton,
  PizzaBlock,
  Pagination,
} from "../components";
import { sortList } from "../components/Sort";
import { selectFilter, setFilters } from "../redux/slices/filterSlice";
import {
  fetchPizzas,
  selectPizzasData,
  Status,
} from "../redux/slices/pizzasSlice";

import { useSelector } from "react-redux";
import { useAppDispatch } from "../redux/store";

const Home: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch = useAppDispatch();
  const isMounted = React.useRef<boolean>(false);
  const isSearch = React.useRef<boolean>(false);
  const { categoryId, sort, currentPage, search } = useSelector(selectFilter);
  const { items, status } = useSelector(selectPizzasData);

  const getPizzas = () => {
    const sortBy: string = sort.sortProperty.replace("-", "");
    const order: string = sort.sortProperty.includes("-") ? "asc" : "desc";
    const category: string = categoryId > 0 ? `category=${categoryId}` : "";
    const searchValue: string = search ? `search=${search}` : "";

    dispatch(
      fetchPizzas({
        currentPage,
        category,
        sortBy,
        order,
        searchValue,
      })
    );
  };

  // Если первый рендер, то не вшиваем
  React.useEffect(() => {
    if (isMounted.current) {
      const queryString = qs.stringify({
        sortProperty: sort.sortProperty,
        categoryId,
        currentPage,
      });

      setSearchParams(queryString);
    }
    isMounted.current = true;
  }, [categoryId, sort.sortProperty, currentPage]);

  React.useEffect(() => {
    if (
      window.location.search &&
      window.location.search !==
        "?sortProperty=-rating&categoryId=0&currentPage=1"
    ) {
      const params = Object.fromEntries([...searchParams]);
      const sort = sortList.find(
        (obj) => obj.sortProperty === params.sortProperty
      );
      dispatch(
        setFilters({
          ...params,
          sort,
        })
      );
      isSearch.current = true;
    }
  }, []);

  React.useEffect(() => {
    window.scrollTo(0, 0);

    if (!isSearch.current) {
      getPizzas();
    }

    isSearch.current = false;
  }, [categoryId, sort.sortProperty, search, currentPage]);

  const pizzas = items.map((obj: any) => <PizzaBlock key={obj.id} {...obj} />);

  const pizzaSkeletons = [...new Array(6)].map((_, index) => (
    <PizzaSkeleton key={index} />
  ));

  return (
    <div className="container">
      <div className="content__top">
        <Categories />
        <Sort />
      </div>
      <h2 className="content__title">Все пиццы</h2>
      {status === "error" ? (
        <div className="content__error">
          <h2>Ошибка запроса :(</h2>
          <p>Пиццы не найдены. Попробуйте сделать запрос позже.</p>
        </div>
      ) : (
        <div className="content__items">
          {status === "loading" ? pizzaSkeletons : pizzas}
        </div>
      )}

      {status === Status.SUCCESS && <Pagination />}
    </div>
  );
};

export default Home;
